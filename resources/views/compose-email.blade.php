<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">

    <title>Laravel</title>

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>

    <style>
        .error{
            color: red;
        }
    </style>

</head>
<body>

<div class="container">
    <div class="row">

        <div class="col-md-10 col-md-offset-1">
            <form method="post" action="{{\URL::to('/')}}/send/email">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="exampleInputEmail1">To</label>
                    <input type="email" class="form-control" placeholder="Enter email" name ="email">
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    @if ($errors->has('email'))
                        <div class="error">{{ $errors->first('email') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Subject</label>
                    <input type="text" class="form-control"  aria-describedby="emailHelp" placeholder="Enter Subject" name ="subject">
                    @if ($errors->has('subject'))
                        <div class="error">{{ $errors->first('subject') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Content</label>
                    <textarea class="form-control" id="exampleInputEmail1" name="body"></textarea>
                    @if ($errors->has('body'))
                        <div class="error">{{ $errors->first('body') }}</div>
                    @endif

                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-sm">Send</button>
                </div>
            </form>

        </div>
    </div>
</div>

</body>
</html>
