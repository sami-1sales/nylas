<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">

    <title>Laravel</title>

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>



</head>
<body>

<div class="container">
    <div class="row">

        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default panel-table">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col col-xs-6">
                            <h3 class="panel-title">Messages</h3>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <h3>{{$messages[0]->subject}}</h3>
                    <table class="table table-striped table-bordered table-list">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Content</th>
                            <th>Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($messages as $message)
                                <tr>
                                    <td >
                                        {{$message->from[0]->name == ''?$message->from[1]->name:$message->from[0]->name}}
                                    </td>
                                    <td>
                                        <p>{!! $message->body !!}</p>
                                    </td>
                                    <td>{{date('d/m/Y', $message->date)}}</td>
                                </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>

            </div>

        </div></div></div>

</body>
</html>
