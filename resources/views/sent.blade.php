<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">

    <title>Laravel</title>

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>



</head>
<body>

<div class="container">
    <div class="row">

        <div class="col-md-10 col-md-offset-1">

            <div class="panel panel-default panel-table">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col col-xs-6">
                            <h3 class="panel-title">Inbox</h3>
                        </div>
                        <div class="col col-xs-6 text-right">
                            <a href="{{\URL::to('/')}}/inbox" class="btn btn-sm btn-primary btn-create">Inbox</a>
                            <a href="{{\URL::to('/')}}/compose/email" class="btn btn-sm btn-primary btn-create">Send Email</a>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-list">
                        <thead>
                        <tr>
                            <th><em class="fa fa-cog"></em></th>
                            <th>Name</th>
                            <th>Content</th>
                            <th>Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($threads as $thread)
                            @if(count($thread->participants))
                            <tr>
                                <td align="center">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                </td>
                                <td style="width:20%">
                                    {{$thread->participants[0]->name == ''?$thread->participants[1]->name:$thread->participants[0]->name}}
                                    <b>{{count($thread->message_ids) > 1?'('.count($thread->message_ids).')':''}}</b>
                                </td>
                                <td><a style="text-decoration: none;color:black;" href="{{\URL::to('/')}}/mail/{{$thread->id}}">{{$thread->subject}} - <small>{{$thread->snippet}}</small></a></td>
                                <td>{{date('d/m/Y', $thread->last_message_timestamp)}}</td>
                            </tr>
                            @endif
                        @endforeach

                        </tbody>
                    </table>

                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col col-xs-4">Page 1 of 5
                        </div>
                        <div class="col col-xs-8">
                            <ul class="pagination hidden-xs pull-right">
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                            </ul>
                            <ul class="pagination visible-xs pull-right">
                                <li><a href="#">«</a></li>
                                <li><a href="#">»</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div></div></div>

</body>
</html>
