<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('authorize', 'NylasController@index');
Route::get('response', 'NylasController@response');
Route::get('revoke', 'NylasController@revoke');
Route::get('account-list', 'NylasController@accountList');
Route::get('account-info', 'NylasController@accountInfo');
Route::get('account-cancel', 'NylasController@accountCancel');
Route::get('account-reactivate', 'NylasController@accountReactivate');
Route::get('account', 'NylasController@account');


Route::get('thread-list', 'NylasController@threadList');
Route::get('thread', 'NylasController@thread');
Route::get('thread-classify', 'NylasController@threadClassify');


Route::get('messages-list', 'NylasController@messageList');
Route::get('message', 'NylasController@message');
Route::get('message-classify', 'NylasController@messageClassify');
Route::get('raw-message', 'NylasController@rawMessage');
Route::get('folder-list', 'NylasController@folderList');
Route::get('folder', 'NylasController@folder');
Route::get('folder-add', 'NylasController@folderAdd');
Route::get('folder-namechange', 'NylasController@folderNameChange');
Route::get('folder-delete', 'NylasController@folderDelete');
Route::get('label-list', 'NylasController@labelList');
Route::get('label', 'NylasController@label');
Route::get('label-add', 'NylasController@labelAdd');
Route::get('label-namechange', 'NylasController@labelNameChange');
Route::get('label-delete', 'NylasController@labelDelete');
Route::get('draft-list', 'NylasController@draftList');
Route::get('draft', 'NylasController@draft');
Route::get('draft-add', 'NylasController@draftAdd');
Route::get('draft-edit', 'NylasController@draftEdit');
Route::get('draft-delete', 'NylasController@draftDelete');
Route::get('send-draft', 'NylasController@sendDraft');
Route::get('send-mail', 'NylasController@sendMail');
Route::get('send-raw', 'NylasController@sendRawMail');
Route::get('file-list', 'NylasController@fileList');
Route::get('file', 'NylasController@file');
Route::get('file-download', 'NylasController@fileDownload');
Route::get('file-upload', 'NylasController@fileUpload');
Route::get('file-delete', 'NylasController@fileDelete');

Route::get('calendar-list', 'NylasController@calendarList');
Route::get('calendar', 'NylasController@calendar');

Route::get('search-messages', 'NylasController@searchMessages');
Route::get('search-threads', 'NylasController@searchThreads');
Route::get('imap-gmail', 'NylasController@imapGmail');
Route::get('imap-general', 'NylasController@imapGeneral');
Route::get('imap-token', 'NylasController@getTokenByCode');
Route::get('imap-generic', 'NylasController@imapGeneric');

//for inbox
Route::get('inbox', 'ThreadController@inbox');
Route::get('sent', 'ThreadController@sent');
Route::get('mail/{id}', 'ThreadController@getMessagesByThreadId');
Route::get('compose/email', 'MailController@composeEmail');
Route::post('send/email', 'MailController@sendEmail');