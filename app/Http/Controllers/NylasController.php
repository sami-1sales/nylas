<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class NylasController extends Controller
{
	protected $nylasUrl = 'https://api.nylas.com';
	protected $appId = '67nomb3xomh3bko2u58t8w9cw';
	protected $appSecret = 'atx9buytakt3yt0qnjxzp7ab9';

	//yahoo token 3nRpwquMbDXhCcpVithuSOidhoBAwo
    //hotmail token PxPfwP4urUpu2SmuHkmPAFwM4qvPsN
    //icloud token bvOFvUHicHRYZzGjztv6cwRm4YDWhL

    public function index()
    {
    	$parameter['client_id'] = $this->appId;
    	$parameter['response_type'] = 'token';
    	$parameter['scope'] = 'email';
        $parameter['login_hint'] = 'sami99978@gmail.com';
    	$parameter['redirect_uri'] = 'http://localhost/nylas/public/response';
        $parameter['redirect_uri'] = 'http://nylas.test/response';

    	$route = '/oauth/authorize';
    	$url = $this->nylasUrl.$route;
    	$client = new Client();
		$request = $client->request('GET', $url, ['query' => $parameter]);

		$body = $request->getBody();
		echo $body;
    }

    public function imapGmail()
    {
        $settings =  [
            "google_client_id" =>    "897564103977-h1kmho4kgi9e097v9k5cd01j97t1lc4v.apps.googleusercontent.com",
            "google_client_secret" => "xX3XjIo8-PoeR0on85r11DCj",
            "google_refresh_token" => "1/KI3HA6IQtBephLYijymLRI1XRfUP0mohNBBCgr0LZuk"
        ];

        $body['client_id'] = $this->appId;
        $body['name'] = 'Samiuzzaman Khan';
        $body['email_address'] = 'sami@1sales.io';
        $body['provider'] = 'gmail';
        $body['settings'] = $settings;

        $body = json_encode($body);

        $request_type = 'POST';
        $route = '/connect/authorize';
        $url = $this->nylasUrl.$route;

        $client = new Client();


        //try {
            $request = $client->request($request_type, $url, ['body' => $body]);
       /* } catch(\GuzzleHttp\Exception\ServerException  $e) {
            dd($e->getResponse()->getBody()->getContents());
        }*/

        $body = $request->getBody();
        dd(json_decode($body));
    }

    public function imapGeneral()
    {
        $settings =  [
            //'username' => "Samiuzzaman Khan",
            "password" =>    "hxub-ffsj-xykf-jczo"
        ];

        $body['client_id'] = $this->appId;
        $body['name'] = 'Samiuzzaman Khan';
        $body['email_address'] =  'sami.1sales@icloud.com';//'sami1sale@hotmail.com';//'sami1sale@yahoo.com';
        $body['provider'] = 'icloud';//;'hotmail';//'yahoo';
        $body['settings'] = $settings;

        $body = json_encode($body);

        $request_type = 'POST';
        $route = '/connect/authorize';
        $url = $this->nylasUrl.$route;

        $client = new Client();


        try {
            $request = $client->request($request_type, $url, ['body' => $body]);
        } catch(\GuzzleHttp\Exception\ServerException  $e) {
             dd($e->getResponse()->getBody()->getContents());
        }

        $body = $request->getBody();
        dd(json_decode($body));
    }

    public function imapGeneric(){
        /*$settings =  [
            "imap_host" =>     "imap.gmail.com",
            "imap_port" =>     993,
            "imap_username" => "sami99978@gmail.com",
            "imap_password" => "",
            "smtp_host" =>   "smtp.gmail.com",
            "smtp_port" =>     465,
            "smtp_username" => "sami99978@gmail.com",
            "smtp_password" => "",
            "ssl_required" =>  true
        ];

        $body['client_id'] = $this->appId;
        $body['name'] = 'Sami Khan';
        $body['email_address'] = 'sami99978@gmail.com';
        $body['provider'] = 'imap';
        $body['settings'] = $settings;*/

        $settings =  [
            "imap_host" =>     "imap.mail.me.com",
            "imap_port" =>     993,
            "imap_username" => "sami.1sales@icloud.com",
            "imap_password" => "bqwr-xirl-rskv-ndwg",
            "smtp_host" =>   "smtp.mail.me.com",
            "smtp_port" =>     587,
            "smtp_username" => "sami.1sales@icloud.com",
            "smtp_password" => "bqwr-xirl-rskv-ndwg",
            "ssl_required" =>  true
        ];

        $body['client_id'] = $this->appId;
        $body['name'] = 'Samiuzzaman Khan';
        $body['email_address'] = 'sami.1sales@icloud.com';
        $body['provider'] = 'imap';
        $body['settings'] = $settings;

        $body = json_encode($body);

        $request_type = 'POST';
        $route = '/connect/authorize';
        $url = $this->nylasUrl.$route;

        $client = new Client();


        try {
            $request = $client->request($request_type, $url, ['body' => $body]);
         } catch(Exception  $e) {
             dd($e->getResponse()->getBody()->getContents());
         }

        $body = $request->getBody();
        dd(json_decode($body));
    }

    public  function getTokenByCode(){

        $body['client_id'] = $this->appId;
        $body['client_secret'] = $this->appSecret;
        $body['code'] = 'LDGQGlyzftDo0tKvzIcHCoWHmHhNOI';//h - '9nNS3gUHJh12yC7dNphuw5CbJfja4Y';  //yahoo - '32u672hql3eKO4GWnIswCAmX13DXBY';//'NxFGjNcc4QAoaJwxV1h4j9t4N1dXTq';

        $body = json_encode($body);

        $request_type = 'POST';
        $route = '/connect/token';
        $url = $this->nylasUrl.$route;

        $client = new Client();


        //try {
        $request = $client->request($request_type, $url, ['body' => $body]);
        /* } catch(\GuzzleHttp\Exception\ServerException  $e) {
             dd($e->getResponse()->getBody()->getContents());
         }*/

        $body = $request->getBody();
        dd(json_decode($body));
    }

    public function authorizeWithImap(){

    }

    public function response()
    {
    	echo "hello";
    }

    public function revoke()
    {
    	$accessToken = 'pQWPf0mS1d6qf5caqaV95qHmSUHKj2'.':';

    	$headers['authorization'] = 'Basic '.base64_encode($accessToken);

    	$request_type = 'POST';
    	$route = '/oauth/revoke';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		echo $body;
    }

    public function account()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

    	$request_type = 'GET';
    	$route = '/account';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		echo $body;
    }

    public function accountList()
    {
    	$accessToken = $this->appSecret.':';

    	$headers['authorization'] = 'Basic '.base64_encode($accessToken);

    	$request_type = 'GET';
    	$route = '/a'.'/'.$this->appId.'/accounts';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		echo $body;
    }

    public function accountInfo()
    {
    	$accessToken = $this->appSecret.':';

    	$headers['authorization'] = 'Basic '.base64_encode($accessToken);

    	$account_id = 'c55f5wnp3gg4y3vpne4tog656';

    	$request_type = 'GET';
    	$route = '/a'.'/'.$this->appId.'/accounts'.'/'.$account_id;
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		echo $body;
    }

    public function accountCancel()
    {
    	$accessToken = $this->appSecret.':';

    	$headers['authorization'] = 'Basic '.base64_encode($accessToken);

    	$account_id = 'd3ct44l0aj413iu41mk55cma7';

    	$request_type = 'POST';
    	$route = '/a'.'/'.$this->appId.'/accounts'.'/'.$account_id.'/downgrade';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		echo $body;
    }

    public function accountReactivate()
    {
    	$accessToken = $this->appSecret.':';

    	$headers['authorization'] = 'Basic '.base64_encode($accessToken);

    	$account_id = 'd3ct44l0aj413iu41mk55cma7';

    	$request_type = 'POST';
    	$route = '/a'.'/'.$this->appId.'/accounts'.'/'.$account_id.'/upgrade';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		echo $body;
    }

    public function threadList()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

    	$request_type = 'GET';
    	$route = '/threads';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		dd(json_decode($body));
    }

    public function thread()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';
    	$thread_id = '2dlixlczjvhjcz3z4pqj939u3';

    	$request_type = 'GET';
    	$route = '/threads'.'/'.$thread_id;
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		dd(json_decode($body));
    }

    public function threadClassify()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';
    	$thread_id = '2dlixlczjvhjcz3z4pqj939u3';

    	$body['unread'] = true;
    	$body['starred'] = true;
    	//$body['folder_id'] = 'string';
    	//$body['label_ids'] = 'array of strings';

    	$request_type = 'PUT';
    	$route = '/threads'.'/'.$thread_id;
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'body' => json_encode($body)]);

		$body = $request->getBody();
		dd(json_decode($body));
    }

    public function messageList()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

    	// $parameter['view'] = 'string ids, count, or expanded';
    	$parameter['limit'] = 10;
    	// $parameter['offset'] = 2;
    	// $parameter['subject'] = 'string';
    	// $parameter['any_email'] = 'string';
    	// $parameter['to'] = 'string';
    	// $parameter['from'] = 'string';
    	// $parameter['cc'] = 'string';
    	// $parameter['bcc'] = 'string';
    	// $parameter['in'] = 'supports the name, display_name, or id of a folder or label';
    	// $parameter['unread'] = boolean;
    	// $parameter['starred'] = boolean;
    	// $parameter['filename'] = 'string';
    	// $parameter['thread_id'] = 'string';
    	// $parameter['received_before'] = 'string unix timestamp';
    	// $parameter['received_after'] = 'string unix timestamp';
    	// $parameter['has_attachment'] = boolean;

    	$request_type = 'GET';
    	$route = '/messages';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'query' => $parameter]);

		$body = $request->getBody();
		dd(json_decode($body));
    }

    public function message()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';
    	$message_id = '5hk2u2frnexghxipkk9w9b7te';

    	$request_type = 'GET';
    	$route = '/messages'.'/'.$message_id;
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		dd(json_decode($body));
    }

    public function messageClassify()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';
    	$message_id = '5hk2u2frnexghxipkk9w9b7te';

    	$body['unread'] = true;
    	$body['starred'] = true;

    	$request_type = 'PUT';
    	$route = '/messages'.'/'.$message_id;
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'body' => json_encode($body)]);

		$body = $request->getBody();
		dd(json_decode($body));
    }

    public function rawMessage()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';
    	$headers['accept'] = 'message/rfc822';
    	$message_id = '5hk2u2frnexghxipkk9w9b7te';

    	$request_type = 'GET';
    	$route = '/messages'.'/'.$message_id;
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		echo $body;
    }

    public function folderList()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

    	$request_type = 'GET';
    	$route = '/folders';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		echo $body;
    }

    public function folder()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';
    	$folder_id = '';

    	$request_type = 'GET';
    	$route = '/folders'.'/'.$folder_id;
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		echo $body;
    }

    public function folderAdd()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

    	$body['display_name'] = 'Project';
    	$body = json_encode($body);

    	$request_type = 'POST';
    	$route = '/folders';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'body' => $body]);

		$body = $request->getBody();
		echo $body;
    }

    public function folderNameChange()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

    	$folder_id = '';

    	$body['display_name'] = 'Project1';
    	$body = json_encode($body);

    	$request_type = 'PUT';
    	$route = '/folders'.'/'.$folder_id;
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'body' => $body]);

		$body = $request->getBody();
		echo $body;
    }

    public function folderDelete()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

    	$folder_id = '';

    	$request_type = 'DELETE';
    	$route = '/folders'.'/'.$folder_id;
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		echo $body;
    }

    public function labelList()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

    	$request_type = 'GET';
    	$route = '/labels';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		echo $body;
    }
    

    public function label()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

    	$label_id = '61nbp5gnzetlnvr9u65yj4g3k';

    	$request_type = 'GET';
    	$route = '/labels'.'/'.$label_id;
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		echo $body;
    }

    public function labelAdd()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

    	$body['display_name'] = 'Project2';
    	$body = json_encode($body);

    	$request_type = 'POST';
    	$route = '/labels';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'body' => $body]);

		$body = $request->getBody();
		echo $body;
    }

    public function labelNameChange()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

    	$label_id = '55xsk0y5ytv5hlwhfc9juerej';

    	$body['display_name'] = 'Project3';
    	$body = json_encode($body);

    	$request_type = 'PUT';
    	$route = '/labels'.'/'.$label_id;
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'body' => $body]);

		$body = $request->getBody();
		echo $body;
    }

    public function labelDelete()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

    	$label_id = '2diigl5egwit6mtjlydxbxabt';

    	$request_type = 'DELETE';
    	$route = '/labels'.'/'.$label_id;
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		echo $body;
    }

    public function draftList()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

    	$request_type = 'GET';
    	$route = '/drafts';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		dd(json_decode($body));
    }
    

    public function draft()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

    	$draft_id = '9hq7l9a7rj3pm9qa0v4jouxov';

    	$request_type = 'GET';
    	$route = '/drafts'.'/'.$draft_id;
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		echo $body;
    }

    public function draftAdd()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

    	//$body['subject'] = 'string';
    	//$body['to'] = 'array of objects';
    	//$body['cc'] = 'array of objects';
    	//$body['bcc'] = 'array of objects';
    	//$body['from'] = 'array of objects';
    	//$body['reply_to'] = 'array of objects';
    	$body['body'] = 'draft test 1';
    	//$body['file_ids'] = 'array of strings';
    	$body = json_encode($body);

    	$request_type = 'POST';
    	$route = '/drafts';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'body' => $body]);

		$body = $request->getBody();
		echo $body;
    }

    public function draftEdit() // causing some problem in gmail
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

    	$draft_id = 'xhpnusseviafq41c5nsw6mci';//'40k54uuulwt8ve2w3tofmr4cm';

    	//$body['subject'] = 'string';
    	$body['to'] = 'sami.cseseu@gmail.com';
    	//$body['cc'] = 'array of objects';
    	//$body['bcc'] = 'array of objects';
    	//$body['from'] = 'array of objects';
    	//$body['reply_to'] = 'array of objects';
    	$body['body'] = 'draft test 3';
    	//$body['file_ids'] = 'array of strings';
    	$body['version'] = '0';
    	$body = json_encode($body);

    	$request_type = 'PUT';
    	$route = '/drafts'.'/'.$draft_id;
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'body' => $body]);

		$body = $request->getBody();
		echo $body;
    }

    public function draftDelete()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

    	$body['version'] = '0';
    	$body = json_encode($body);

    	$draft_id = 'abdzmatx7y5u03ty1regd5wi6';

    	$request_type = 'DELETE';
    	$route = '/drafts'.'/'.$draft_id;
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'body' => $body]);

		$body = $request->getBody();
		echo $body;
    }

    public function sendDraft()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

    	//$body['subject'] = 'string';
    	//$body['to'] = ['name' => 'sami','email' => 'sami.cseseu@gmail.com'];
    	//$body['cc'] = 'array of objects';
    	//$body['bcc'] = 'array of objects';
    	//$body['from'] = 'array of objects';
    	//$body['reply_to'] = 'array of objects';
    	
    	//$body['file_ids'] = 'array of strings';
    	$body['draft_id'] = 'xhpnusseviafq41c5nsw6mci';
    	$body['version'] = '0';
    	$body = json_encode($body);

    	$request_type = 'POST';
    	$route = '/send';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'body' => $body]);

		$body = $request->getBody();
		echo $body;
    }

    public function sendMail()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

    	$body['subject'] = 'Test Email Nylas';
    	//$body['reply_to_message_id'] = int32;
    	//$body['from'] = 'array of objects';
    	//$body['reply_to'] = 'array of objects';
    	$body['to'] = [['name' => 'sami','email' => 'sami.cseseu@gmail.com']]; //'array of objects';
    	//$body['cc'] = 'array of objects';
    	//$body['bcc'] = 'array of objects';
    	$body['body'] = 'test text';
    	//$body['file_ids'] = 'array of strings';
    	//$body['tracking'] = 'object';
    	$body = json_encode($body);

    	$request_type = 'POST';
    	$route = '/send';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'body' => $body]);

		$body = $request->getBody();
		echo $body;
    }

    public function sendRawMail() // problem
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';
    	$headers['content-Type'] = 'text/plain';

    	$body = 'Subject: Meeting on Thursday
				From: Bill <sami@1sales.io>
				To: Ben Bitdiddle <sami.cseseu@gmail.com>
				
				Hey Ben,';

		//In-Reply-To: <84umizq7c4jtrew491brpa6iu-0@mailer.nylas.com>
		//References: <84umizq7c4jtrew491brpa6iu-0@mailer.nylas.com>

    	$request_type = 'POST';
    	$route = '/send';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'body' => $body]);

		$body = $request->getBody();
		echo $body;
    }

    public function fileList() // file serach mainly
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

    	$parameter['filename'] = 'nylas.txt';
    	//$parameter['message_id'] = 'token';
    	//$parameter['content_type'] = 'text/plain';
    	//$parameter['view'] = 'sami.cseseu@gmail.com';

    	$request_type = 'GET';
    	$route = '/files';
    	$url = $this->nylasUrl.$route;
    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'query' => $parameter]);

		$body = $request->getBody();
		echo $body;
    }

    public function file()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

    	$file_id = '1c6wu13ublab7gkysruwvbop5';

    	$request_type = 'GET';
    	$route = '/files'.'/'.$file_id;
    	$url = $this->nylasUrl.$route;
    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		echo $body;
    }

    public function fileDownload()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

    	$file_id = '1c6wu13ublab7gkysruwvbop5';

    	$request_type = 'GET';
    	$route = '/files'.'/'.$file_id.'/download';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		echo $body;
    } 

    public function fileUpload()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

    	$file_path = 'D:\nylas.txt';

    	$body = [
			        'file' => [
			            'name'     => 'test',
			            'contents' => fopen($file_path, 'r'),
			        ]
			    ];

    	$request_type = 'POST';
    	$route = '/files';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'multipart' => $body]);

		$body = $request->getBody();
		echo $body;
    }

    public function fileDelete()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

    	$file_id = '1yhjrq325gnbrj3s6j1bda8qu';

    	$request_type = 'DELETE';
    	$route = '/files'.'/'.$file_id;
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		echo $body;
    } 

    public function calendarList()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

    	$parameter['view'] = 'ids'; // count or ids
    	//$parameter['limit'] = 'string';
    	//$parameter['offset'] = 'string';

    	$route = '/calendars';
    	$url = $this->nylasUrl.$route;
    	$client = new Client();
		$request = $client->request('GET', $url, ['headers' => $headers, 'query' => $parameter]);

		$body = $request->getBody();
		echo $body;
    } 

    public function calendar()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

   		$calendar_id = 'diow5jb8n6nlnxuus5qym89ga';

    	$route = '/calendars'.'/'.$calendar_id;
    	$url = $this->nylasUrl.$route;
    	$client = new Client();
		$request = $client->request('GET', $url, ['headers' => $headers]);

		$body = $request->getBody();
		echo $body;
    }

    /*public function eventList()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

   		$calendar_id = 'diow5jb8n6nlnxuus5qym89ga';

    	$route = '/calendars'.'/'.$calendar_id;
    	$url = $this->nylasUrl.$route;
    	$client = new Client();
		$request = $client->request('GET', $url, ['headers' => $headers]);

		$body = $request->getBody();
		echo $body;
    } 

     public function event()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

   		$calendar_id = 'diow5jb8n6nlnxuus5qym89ga';

    	$route = '/calendars'.'/'.$calendar_id;
    	$url = $this->nylasUrl.$route;
    	$client = new Client();
		$request = $client->request('GET', $url, ['headers' => $headers]);

		$body = $request->getBody();
		echo $body;
    }*/

    public function searchMessages()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

    	$parameter['q'] = 'Nylas';
    	
    	$request_type = 'GET';
    	$route = '/messages/search';
    	$url = $this->nylasUrl.$route;
    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'query' => $parameter]);

		$body = $request->getBody();
		dd(json_decode($body));
    }

    public function searchThreads()
    {
    	$headers['authorization'] = 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

    	$parameter['q'] = 'Nylas';

    	$request_type = 'GET';
    	$route = '/threads/search';
    	$url = $this->nylasUrl.$route;
    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'query' => $parameter]);

		$body = $request->getBody();
		dd(json_decode($body));
    }


}
