<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Http\Controllers\Nylas\Nylas;
use Validator;

class MailController extends Controller
{
    protected $appId = '67nomb3xomh3bko2u58t8w9cw';
    protected $appSecret = 'atx9buytakt3yt0qnjxzp7ab9';
    protected $token = '3nRpwquMbDXhCcpVithuSOidhoBAwo';

    protected  $nylas;

    public function __construct()
    {
        $this->nylas = new Nylas($this->appId,$this->appSecret);

    }

    public function composeEmail()
    {
        return view('compose-email');
    }

    public function sendEmail(Request $request)
    {
        $rules = [
            'email' =>'required|email',
            'subject' => 'required',
            'body' => 'required'
        ];

        $attributeNames = [
            'email' => 'Email',
            'subject' => 'Subject',
            'body' => 'Content'
        ];

        $validator = Validator::make ( $request->all(), $rules );
        $validator->setAttributeNames($attributeNames);

        if($validator->validate()){
            return \Redirect::back()->withInput()->withErrors($validator);
        }

        $parameters['subject'] = $request->subject;
        //$parameters['reply_to_message_id'] = int32;
        //$parameters['from'] = 'array of objects';
        //$parameters['reply_to'] = 'array of objects';
        $parameters['to'] = [['email' => $request->email]]; //'array of objects';
        //$parameters['cc'] = 'array of objects';
        //$parameters['bcc'] = 'array of objects';
        $parameters['body'] = $request->body;
        //$parameters['file_ids'] = 'array of strings';
        //$parameters['tracking'] = 'object';

        $mail = $this->nylas->mail($this->token);
        $mail->sendMail($parameters);

        return redirect('/inbox');

    }


}
