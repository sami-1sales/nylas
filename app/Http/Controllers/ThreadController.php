<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use App\Http\Controllers\Nylas\Nylas;

class ThreadController extends Controller
{
	protected $appId = '67nomb3xomh3bko2u58t8w9cw';
	protected $appSecret = 'atx9buytakt3yt0qnjxzp7ab9';
	protected $token = '3nRpwquMbDXhCcpVithuSOidhoBAwo';//g 'NXbFpU58NEdfSx2UwaTNrPQkiRusVT';

    protected  $nylas;

    public function __construct()
    {
        $this->nylas = new Nylas($this->appId,$this->appSecret);

    }

    public function inbox()
    {
        $message = $this->nylas->message($this->token);
        $account = $this->nylas->account();

        $accountInfo = json_decode($account->getAccountByToken($this->token));
        $parameters['to'] = $accountInfo->email_address;

        $parameters['limit'] = 40;


        $threads = $message->getThreads($parameters);

        $data['threads'] = json_decode($threads);

        return view('inbox', $data);
    }

    public function sent()
    {
        $message = $this->nylas->message($this->token);
        $account = $this->nylas->account();

        $accountInfo = json_decode($account->getAccountByToken($this->token));
        $parameters['from'] = $accountInfo->email_address;

        $parameters['limit'] = 40;


        $threads = $message->getThreads($parameters);

        $data['threads'] = json_decode($threads);

        return view('sent', $data);
    }

    public function getMessagesByThreadId(Request $request)
    {
        $parameters['thread_id'] = $request->id;
        $parameters['view'] = 'expanded';

        $message = $this->nylas->message($this->token);
        $messages= $message->getMessages($parameters);

        $data['messages'] = json_decode($messages);

        //dd($data);

        return view('mail', $data);
    }


}
