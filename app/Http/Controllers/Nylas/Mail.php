<?php

namespace App\Http\Controllers\Nylas;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class Mail
{
    protected $nylasUrl;
    protected $appId;
    protected $appSecret;
    protected $accessToken;

    public function __construct($nylasUrl, $appId, $appSecret, $accessToken)
    {
        $this->nylasUrl = $nylasUrl;
        $this->appId = $appId;
        $this->appSecret = $appSecret;
        $this->accessToken = $accessToken;

    }

	public function sendDraftById($draftId, $draftVersion, $mailParameters = [])
    {
    	$headers['authorization'] = $this->accessToken;
    	$body = $mailParameters;

    	$body['draft_id'] = $draftId;
    	$body['version'] = $draftVersion;
    	$body = json_encode($body);

    	$request_type = 'POST';
    	$route = '/send';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'body' => $body]);

		$body = $request->getBody();
		return $body;
    }

    public function sendMail($mailParameters)
    {
    	$headers['authorization'] = $this->accessToken;

    	$body = json_encode($mailParameters);

    	$request_type = 'POST';
    	$route = '/send';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'body' => $body]);

		$body = $request->getBody();
        return $body;
    }

    public function sendRawMail($mailBodyText) // problem
    {
    	$headers['authorization'] = $this->accessToken;
    	$headers['content-Type'] = 'text/plain';

    	$body = $mailBodyText;

		//In-Reply-To: <84umizq7c4jtrew491brpa6iu-0@mailer.nylas.com>
		//References: <84umizq7c4jtrew491brpa6iu-0@mailer.nylas.com>

    	$request_type = 'POST';
    	$route = '/send';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'body' => $body]);

		$body = $request->getBody();
		return $body;
    }

}