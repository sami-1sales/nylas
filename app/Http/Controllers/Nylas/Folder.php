<?php

namespace App\Http\Controllers\Nylas;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class Folder
{
    protected $nylasUrl;
    protected $appId;
    protected $appSecret;
    protected $accessToken;

    public function __construct($nylasUrl, $appId, $appSecret, $accessToken)
    {
        $this->nylasUrl = $nylasUrl;
        $this->appId = $appId;
        $this->appSecret = $appSecret;
        $this->accessToken = $accessToken;

    }

    public function getFolders()
    {
        $headers['authorization'] = $this->accessToken;

        $request_type = 'GET';
        $route = '/folders';
        $url = $this->nylasUrl.$route;

        $client = new Client();
        $request = $client->request($request_type, $url, ['headers' => $headers]);

        $body = $request->getBody();
        return $body;
    }

    public function getFolder($folderId)
    {
        $headers['authorization'] = $this->accessToken;

        $request_type = 'GET';
        $route = '/folders'.'/'.$folderId;
        $url = $this->nylasUrl.$route;

        $client = new Client();
        $request = $client->request($request_type, $url, ['headers' => $headers]);

        $body = $request->getBody();
        return $body;
    }

    public function createFolderByName($displayName)
    {
        $headers['authorization'] = $this->accessToken;

        $body['display_name'] = $displayName;
        $body = json_encode($body);

        $request_type = 'POST';
        $route = '/folders';
        $url = $this->nylasUrl.$route;

        $client = new Client();
        $request = $client->request($request_type, $url, ['headers' => $headers, 'body' => $body]);

        $body = $request->getBody();
        return $body;
    }

    public function changeFolderName($folderId, $displayName)
    {
        $headers['authorization'] = $this->accessToken;

        $body['display_name'] = $displayName;
        $body = json_encode($body);

        $request_type = 'PUT';
        $route = '/folders'.'/'.$folderId;
        $url = $this->nylasUrl.$route;

        $client = new Client();
        $request = $client->request($request_type, $url, ['headers' => $headers, 'body' => $body]);

        $body = $request->getBody();
        return $body;
    }

    public function deleteFolderById($folderId)
    {
        $headers['authorization'] = $this->accessToken;

        $request_type = 'DELETE';
        $route = '/folders'.'/'.$folderId;
        $url = $this->nylasUrl.$route;

        $client = new Client();
        $request = $client->request($request_type, $url, ['headers' => $headers]);

        $body = $request->getBody();
        return $body;
    }
}