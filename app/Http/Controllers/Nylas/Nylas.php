<?php

namespace App\Http\Controllers\Nylas;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

//Nylas classes
use App\Http\Controllers\Nylas\Account;
use App\Http\Controllers\Nylas\Calender;
use App\Http\Controllers\Nylas\Draft;
use App\Http\Controllers\Nylas\File;
use App\Http\Controllers\Nylas\Folder;
use App\Http\Controllers\Nylas\Label;
use App\Http\Controllers\Nylas\Mail;
use App\Http\Controllers\Nylas\Message;


class Nylas
{
    protected $nylasUrl = 'https://api.nylas.com';
    protected $appId;
    protected $appSecret;


    public function __construct($appId, $appSecret)
    {
        $this->appId = $appId;
        $this->appSecret = $appSecret;

    }

    public function account()
    {
        return new Account($this->nylasUrl, $this->appId, $this->appSecret);
    }

    public function calender($token)
    {
        return new Calender($this->nylasUrl, $this->appId, $this->appSecret, $token);
    }

    public function draft($token)
    {
        return new Draft($this->nylasUrl, $this->appId, $this->appSecret, $token);
    }

    public function file($token)
    {
        return new File($this->nylasUrl, $this->appId, $this->appSecret, $token);
    }

    public function folder($token)
    {
        return new Folder($this->nylasUrl, $this->appId, $this->appSecret, $token);
    }

    public function label($token)
    {
        return new Label($this->nylasUrl, $this->appId, $this->appSecret, $token);
    }

    public function mail($token)
    {
        return new Mail($this->nylasUrl, $this->appId, $this->appSecret, $token);
    }

    public function message($token)
    {
        return new Message($this->nylasUrl, $this->appId, $this->appSecret, $token);
    }


}