<?php

namespace App\Http\Controllers\Nylas;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class Account
{
    protected $nylasUrl;
    protected $appId;
    protected $appSecret;



    public function __construct($nylasUrl, $appId, $appSecret)
    {
        $this->nylasUrl = $nylasUrl;
        $this->appId = $appId;
        $this->appSecret = $appSecret;

    }

	public function authorize()
    {
    	$parameter['client_id'] = $this->appId;
    	$parameter['response_type'] = 'token';
    	$parameter['scope'] = 'email';
    	//$parameter['login_hint'] = 'sami.cseseu@gmail.com';
    	$parameter['redirect_uri'] = 'http://localhost/nylas/public/response';

    	$route = '/oauth/authorize';
    	$url = $this->nylasUrl.$route;
    	$client = new Client();
		$request = $client->request('GET', $url, ['query' => $parameter]);

		$body = $request->getBody();
		return $body;
    }



    public function revokeToken($accessToken)
    {

    	$headers['authorization'] = 'Basic '.base64_encode($accessToken);

    	$request_type = 'POST';
    	$route = '/oauth/revoke';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		return $body;
    }

    public function getAccountByToken($accessToken)
    {
    	$headers['authorization'] = $accessToken;

    	$request_type = 'GET';
    	$route = '/account';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		return $body;
    }

    public function getAccounts()
    {
    	$accessToken = $this->appSecret.':';

    	$headers['authorization'] = 'Basic '.base64_encode($accessToken);

    	$request_type = 'GET';
    	$route = '/a'.'/'.$this->appId.'/accounts';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		return $body;
    }

    public function getAccountById($accountId)
    {
    	$accessToken = $this->appSecret.':';

    	$headers['authorization'] = 'Basic '.base64_encode($accessToken);

    	$request_type = 'GET';
    	$route = '/a'.'/'.$this->appId.'/accounts'.'/'.$accountId;
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		return $body;
    }

    public function cancelAccountById($accountId)
    {
    	$accessToken = $this->appSecret.':';

    	$headers['authorization'] = 'Basic '.base64_encode($accessToken);

    	$request_type = 'POST';
    	$route = '/a'.'/'.$this->appId.'/accounts'.'/'.$accountId.'/downgrade';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		return $body;
    }

    public function reactivateAccountById($accountId)
    {
    	$accessToken = $this->appSecret.':';

    	$headers['authorization'] = 'Basic '.base64_encode($accessToken);

    	$request_type = 'POST';
    	$route = '/a'.'/'.$this->appId.'/accounts'.'/'.$accountId.'/upgrade';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		return $body;
    }
}