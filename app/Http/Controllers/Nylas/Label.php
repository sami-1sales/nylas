<?php

namespace App\Http\Controllers\Nylas;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class Label
{

    protected $nylasUrl;
    protected $appId;
    protected $appSecret;
    protected $accessToken;

    public function __construct($nylasUrl, $appId, $appSecret, $accessToken)
    {
        $this->nylasUrl = $nylasUrl;
        $this->appId = $appId;
        $this->appSecret = $appSecret;
        $this->accessToken = $accessToken;

    }

    public function getLabels()
    {
    	$headers['authorization'] = $this->accessToken;

    	$request_type = 'GET';
    	$route = '/labels';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		return $body;
    }
    

    public function getLabelById($labelId)
    {
    	$headers['authorization'] = $this->accessToken;

    	$request_type = 'GET';
    	$route = '/labels'.'/'.$labelId;
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		return $body;
    }

    public function createLabelByName($displayName)
    {
    	$headers['authorization'] = $this->accessToken;

    	$body['display_name'] = $displayName;
    	$body = json_encode($body);

    	$request_type = 'POST';
    	$route = '/labels';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'body' => $body]);

		$body = $request->getBody();
		return $body;
    }

    public function changeLabelName($labelId, $displayName)
    {
    	$headers['authorization'] = $this->accessToken;

    	$body['display_name'] = $displayName;
    	$body = json_encode($body);

    	$request_type = 'PUT';
    	$route = '/labels'.'/'.$labelId;
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'body' => $body]);

		$body = $request->getBody();
		return $body;
    }

    public function deleteLabelById($labelId)
    {
    	$headers['authorization'] = $this->accessToken;

    	$request_type = 'DELETE';
    	$route = '/labels'.'/'.$labelId;
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		return $body;
    }
}