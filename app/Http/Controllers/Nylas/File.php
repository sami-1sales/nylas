<?php

namespace App\Http\Controllers\Nylas;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class File
{
    protected $nylasUrl;
    protected $appId;
    protected $appSecret;
    protected $accessToken;

    public function __construct($nylasUrl, $appId, $appSecret, $accessToken)
    {
        $this->nylasUrl = $nylasUrl;
        $this->appId = $appId;
        $this->appSecret = $appSecret;
        $this->accessToken = $accessToken;

    }

    public function getFiles($parameters) // file search mainly
    {
    	$headers['authorization'] = $this->accessToken;

    	$request_type = 'GET';
    	$route = '/files';
    	$url = $this->nylasUrl.$route;
    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'query' => $parameters]);

		$body = $request->getBody();
		return $body;
    }

    public function getFileById($fileId)
    {
    	$headers['authorization'] = $this->accessToken;

    	$request_type = 'GET';
    	$route = '/files'.'/'.$fileId;
    	$url = $this->nylasUrl.$route;
    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		return $body;
    }

    public function downloadFileById($fileId)
    {
    	$headers['authorization'] = $this->accessToken;

    	$request_type = 'GET';
    	$route = '/files'.'/'.$fileId.'/download';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		return $body;
    } 

    public function uploadFileByPath($filePath)
    {
    	$headers['authorization'] = $this->accessToken;

    	$body = [
			        'file' => [
			            'name'     => 'test',
			            'contents' => fopen($filePath, 'r'),
			        ]
			    ];

    	$request_type = 'POST';
    	$route = '/files';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'multipart' => $body]);

		$body = $request->getBody();
		return $body;
    }

    public function deleteFileById($fileId)
    {
    	$headers['authorization'] = $this->accessToken;

    	$request_type = 'DELETE';
    	$route = '/files'.'/'.$fileId;
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		return $body;
    }   
}