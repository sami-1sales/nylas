<?php

namespace App\Http\Controllers\Nylas;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class Draft
{
    protected $nylasUrl;
    protected $appId;
    protected $appSecret;
    protected $accessToken;

    public function __construct($nylasUrl, $appId, $appSecret, $accessToken)
    {
        $this->nylasUrl = $nylasUrl;
        $this->appId = $appId;
        $this->appSecret = $appSecret;
        $this->accessToken = $accessToken;

    }

    public function getDrafts()
    {
    	$headers['authorization'] = $this->accessToken;

    	$request_type = 'GET';
    	$route = '/drafts';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		return $body;
    }
    

    public function getDraftsById($draftId)
    {
    	$headers['authorization'] = $this->accessToken;

    	$request_type = 'GET';
    	$route = '/drafts'.'/'.$draftId;
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		return $body;
    }

    public function createDraft($parameters)
    {
    	$headers['authorization'] = $this->accessToken;

    	$body = json_encode($parameters);

    	$request_type = 'POST';
    	$route = '/drafts';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'body' => $body]);

		$body = $request->getBody();
		echo $body;
    }

    public function editDraftById($draftId, $parameters) // causing some problem in gmail
    {
    	$headers['authorization'] = $this->accessToken;

    	$body = json_encode($parameters);

    	$request_type = 'PUT';
    	$route = '/drafts'.'/'.$draftId;
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'body' => $body]);

		$body = $request->getBody();
		return $body;
    }

    public function deleteDraft($draftId, $version)
    {
    	$headers['authorization'] = $this->accessToken;

    	$body['version'] = $version;
    	$body = json_encode($body);

    	$request_type = 'DELETE';
    	$route = '/drafts'.'/'.$draftId;
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'body' => $body]);

		$body = $request->getBody();
		return $body;
    }
}