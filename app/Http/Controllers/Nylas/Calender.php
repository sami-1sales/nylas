<?php

namespace App\Http\Controllers\Nylas;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class Calender
{
    protected $nylasUrl;
    protected $appId;
    protected $appSecret;
    protected $accessToken;

    public function __construct($nylasUrl, $appId, $appSecret, $accessToken)
    {
        $this->nylasUrl = $nylasUrl;
        $this->appId = $appId;
        $this->appSecret = $appSecret;
        $this->accessToken = $accessToken;

    }

    public function getcalendars()
    {
        $headers['authorization'] = $this->accessToken;

        $parameter['view'] = 'ids'; // count or ids
        //$parameter['limit'] = 'string';
        //$parameter['offset'] = 'string';

        $route = '/calendars';
        $url = $this->nylasUrl.$route;
        $client = new Client();
        $request = $client->request('GET', $url, ['headers' => $headers, 'query' => $parameter]);

        $body = $request->getBody();
        echo $body;
    } 

    public function getCalenderById($calendarId)
    {
        $headers['authorization'] = $this->accessToken;

        $route = '/calendars'.'/'.$calendarId;
        $url = $this->nylasUrl.$route;
        $client = new Client();
        $request = $client->request('GET', $url, ['headers' => $headers]);

        $body = $request->getBody();
        echo $body;
    }
}
