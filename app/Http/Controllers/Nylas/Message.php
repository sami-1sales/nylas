<?php

namespace App\Http\Controllers\Nylas;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class Message
{
    protected $nylasUrl;
    protected $appId;
    protected $appSecret;
    protected $accessToken;

    public function __construct($nylasUrl, $appId, $appSecret, $accessToken)
    {
        $this->nylasUrl = $nylasUrl;
        $this->appId = $appId;
        $this->appSecret = $appSecret;
        $this->accessToken = $accessToken;

    }

	public function getThreads($parameters = [])
    {
    	$headers['authorization'] = $this->accessToken;

    	$request_type = 'GET';
    	$route = '/threads';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'query' => $parameters]);

		$body = $request->getBody();
		return $body;
    }

    public function getThreadById($threadId)
    {
    	$headers['authorization'] = $this->accessToken;

    	$request_type = 'GET';
    	$route = '/threads'.'/'.$threadId;
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		dd(json_decode($body));
    }

    public function classifyThread($threadId, $parameters = [])
    {
    	$headers['authorization'] = $this->accessToken;

    	$request_type = 'PUT';
    	$route = '/threads'.'/'.$threadId;
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'body' => json_encode($parameters)]);

		$body = $request->getBody();
		dd(json_decode($body));
    }

    public function searchThreadsByString($query)
    {
        $headers['authorization'] = $this->accessToken;

        $parameter['q'] = $query;

        $request_type = 'GET';
        $route = '/threads/search';
        $url = $this->nylasUrl.$route;
        $client = new Client();
        $request = $client->request($request_type, $url, ['headers' => $headers, 'query' => $parameter]);

        $body = $request->getBody();
        return $body;
    }

    public function getMessages($parameters = [])
    {
    	$headers['authorization'] = $this->accessToken;

    	$request_type = 'GET';
    	$route = '/messages';
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'query' => $parameters]);

		$body = $request->getBody();
		return $body;
    }

    public function getMessageById($messageId)
    {
    	$headers['authorization'] = $this->accessToken;

    	$request_type = 'GET';
    	$route = '/messages'.'/'.$messageId;
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
        return $body;
    }

    public function classifyMessage($messageId, $parameters)
    {
    	$headers['authorization'] = $this->accessToken;

    	$request_type = 'PUT';
    	$route = '/messages'.'/'.$messageId;
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'body' => json_encode($parameters)]);

		$body = $request->getBody();
        return $body;
    }

    public function getRawMessageById($messageId)
    {
    	$headers['authorization'] = $this->accessToken;
    	$headers['accept'] = 'message/rfc822';

    	$request_type = 'GET';
    	$route = '/messages'.'/'.$messageId;
    	$url = $this->nylasUrl.$route;

    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers]);

		$body = $request->getBody();
		return $body;
    }

    public function searchMessagesByString($query)
    {
    	$headers['authorization'] = $this->accessToken;

    	$parameter['q'] = $query;
    	
    	$request_type = 'GET';
    	$route = '/messages/search';
    	$url = $this->nylasUrl.$route;
    	$client = new Client();
		$request = $client->request($request_type, $url, ['headers' => $headers, 'query' => $parameter]);

		$body = $request->getBody();
        return $body;
    }


}